import urllib.request
import re
from datetime import datetime

YYJ_LINK = "https://www.catsa-acsta.gc.ca/en/airport/victoria-international-airport"
#YYZ_LINK = "https://www.catsa-acsta.gc.ca/en/airport/toronto-pearson-international-airport"

TR_REGEXP = "<tr.*>.*<td.*>\s*([A-Za-z0-9\s]*)\s*</td>.*<td.*>\s*([A-Za-z0-9\s\-]*)\s*</td>.*<td.*>.*</td>.*</tr>"

with urllib.request.urlopen(YYJ_LINK) as f:
    blob = f.read().decode('utf-8')
    m = re.search(TR_REGEXP, blob, re.DOTALL)

    with open("out.csv", "a") as fout:
        now = datetime.now()
        if m is not None:
            checkpoint = m.group(1).strip()
            waittime = m.group(2).strip()
            print(now, checkpoint, waittime)
            fout.write(str(now) + "," + str(checkpoint) + "," + str(waittime) + "\n")
        else:
            print(now, "no match")
            fout.write(str(now) + ",no match,\n")
