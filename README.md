# Security Wait Time

A simple script for fetching airport security line wait times published by the Canadian Air Transport Safety Authority (CATSA) on their [website](https://www.catsa-acsta.gc.ca/en/airport/victoria-international-airport). The purpose is to archive the history of wait times and identify trends to help travellers better estimate their arrival time at the airport.

Maybe some day I can turn this into a full webpage... 

## Usage

Requires Python3.x. Run using: 

```
python3 wait-time.py
```

This will fetch the current wait time (for YYJ by default) and append it to a file called `out.csv`. The script can be run periodically using a cron job to build up a history of wait times.
